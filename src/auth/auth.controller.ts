import { Body, Controller, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(
    @Body() loginDto: LoginDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    const auth = await this.authService.validateUser(loginDto);
    if (!auth) {
      res.status(401).json({
        status: false,
        message: 'Unauthorized!',
      });
    }

    res.cookie('jwt', auth, { httpOnly: true });

    res.status(200).json({
      status: true,
      message: 'Success login!',
      data: {
        access_token: auth,
      },
    });
  }
}
