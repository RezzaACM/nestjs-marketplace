import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { LoginDto } from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(loginDto: LoginDto): Promise<any> {
    const user: any = await this.usersService.findEmail(loginDto.email);
    if (!user) return false;

    const isMatch = await bcrypt.compare(loginDto.password, user.password);
    if (!isMatch) return false;

    const jwt = await this.jwtService.signAsync({
      _id: user._id,
      name: user.name,
      contact: user.contact,
    });

    return jwt;
  }
}
