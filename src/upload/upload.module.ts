import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { UploadController } from './upload.controller';

@Module({
  controllers: [UploadController],
  imports: [
    MulterModule.register({
      limits: {
        fileSize: 2048,
      },
    }),
  ],
})
export class UploadModule {}
