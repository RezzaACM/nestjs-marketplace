import {
  Controller,
  Post,
  Req,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import * as AWS from 'aws-sdk';
import { Request, Response } from 'express';

@Controller('upload')
export class UploadController {
  s3 = new AWS.S3({
    accessKeyId: 'AKIA27AQ6QHCRMXT3RWB',
    secretAccessKey: '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',
    region: 'ap-southeast-1',
  });

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @Req() req: Request,
    @Res() res: Response,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<any> {
    console.log(file);
    return;
    if (file.size >= 2097152) {
      res.status(400).json({
        status: false,
        message: 'Size file too large!',
      });
    }
    this.s3Upload(file, req.body.destination)
      .then((result) => {
        res.status(200).json({
          status: true,
          message: 'Upload file success!',
          data: {
            path: result.Location,
            key: result.Key,
          },
        });
      })
      .catch((err) => {
        console.log(err);
        res.status(400).json({
          status: false,
          message: 'Something went wrong!',
        });
      });
  }

  private async s3Upload(file: Express.Multer.File, destination: string) {
    const params = {
      Key: `${destination}/${file.originalname}`,
      Bucket: 'iiws-image-slider',
      Body: file.buffer,
      ACL: 'public-read',
      ContentType: file.mimetype,
    };
    return await this.s3.upload(params).promise();
  }
}
