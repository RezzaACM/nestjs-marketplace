import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Next,
  Param,
  Patch,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { UserCreateDto } from './dto/user-create.dto';
import { UsersService } from './users.service';
import * as bcrypt from 'bcrypt';
import { UserUpdateDto } from './dto/user-update.dto';
import { UserPasswordUpdateDto } from './dto/user-password-update.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post()
  async createUser(
    @Res() res: Response,
    @Body() userCreateDto: UserCreateDto,
  ): Promise<void> {
    const round = 10;
    const hashPassword = await bcrypt.hash(userCreateDto.password, round);
    userCreateDto.password = hashPassword;
    try {
      await this.userService.create(userCreateDto);
      res.status(201).json({
        status: true,
        message: 'Successfully create user!',
      });
    } catch (error) {
      throw new HttpException(error.toString(), HttpStatus.BAD_REQUEST);
    }
  }

  @Get()
  async getUser(@Res() res: Response) {
    try {
      const data = await this.userService.getUser();
      res.status(200).json({
        status: true,
        message: 'Successfully retrieve data!',
        data: data,
        total: data.length,
      });
    } catch (error) {
      throw new HttpException(error.toString(), HttpStatus.BAD_REQUEST);
    }
  }

  @Get(':id')
  async getUserById(@Param('id') id: string, @Res() res: Response) {
    try {
      const data = await this.userService.findOne(id);
      res.status(200).json({
        status: true,
        message: 'Success retrieve data!',
        data: data,
      });
    } catch (error) {
      throw new HttpException(`Data not found!`, HttpStatus.NOT_FOUND);
    }
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: string, @Res() res: Response) {
    try {
      await this.userService.delete(id);
      res.status(200).json({
        status: true,
        message: 'Success delete user!',
      });
    } catch (error) {
      throw new HttpException(error.toString(), HttpStatus.NOT_FOUND);
    }
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Res() res: Response,
    @Body() userUpdateDto: UserUpdateDto,
  ) {
    try {
      await this.userService.update(userUpdateDto, id);
      res.status(200).json({
        status: true,
        message: 'Successfully update user!',
      });
    } catch (error) {
      throw new HttpException(error.toString(), HttpStatus.BAD_REQUEST);
    }
  }

  @Patch('update-password/:id')
  async updatePassword(
    @Param('id') id: string,
    @Res() res: Response,
    @Next() next: NextFunction,
    @Body() userPasswordUpdateDto: UserPasswordUpdateDto,
  ) {
    this.userService
      .findOne(id)
      .then(() => {
        next;
      })
      .catch((err) => {
        res.status(404).json({
          status: false,
          errorMessage: err,
        });
      });

    const hash = await this.userService.findPassword(id);
    const isMatch = await bcrypt.compare(
      userPasswordUpdateDto.password,
      hash.password,
    );

    if (!isMatch) {
      res.status(401).json({
        status: false,
        message: 'Incorrect password!',
      });
    }

    const round = 10;
    const hashPassword = await bcrypt.hash(
      userPasswordUpdateDto.newPassword,
      round,
    );
    userPasswordUpdateDto.newPassword = hashPassword;
    try {
      await this.userService.updatePassword(userPasswordUpdateDto, id);
      res.status(200).json({
        status: true,
        message: 'Successfully update user!',
      });
    } catch (error) {
      throw new HttpException(error.toString(), HttpStatus.BAD_REQUEST);
    }
  }
}
