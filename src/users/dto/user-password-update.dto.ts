import { IsNotEmpty, MinLength } from 'class-validator';

export class UserPasswordUpdateDto {
  @IsNotEmpty()
  @MinLength(3)
  password: string;

  @IsNotEmpty()
  @MinLength(3)
  newPassword: string;
}
