import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class ContactCreateDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  @Length(3, 15)
  phone: number;
}
