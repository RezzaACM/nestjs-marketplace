import { Type } from 'class-transformer';
import { IsNotEmpty, MinLength, ValidateNested } from 'class-validator';
import { ContactCreateDto } from './contact-create.dto';

export class UserCreateDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  gender: string;

  address: string;

  @ValidateNested()
  @Type(() => ContactCreateDto)
  contact!: ContactCreateDto;

  @MinLength(3)
  @IsNotEmpty()
  password: string;
}
