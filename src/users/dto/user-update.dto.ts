import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { ContactCreateDto } from './contact-create.dto';

export class UserUpdateDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  gender: string;

  address: string;

  @ValidateNested()
  @Type(() => ContactCreateDto)
  contact!: ContactCreateDto;
}
