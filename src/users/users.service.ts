import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserCreateDto } from './dto/user-create.dto';
import { UserPasswordUpdateDto } from './dto/user-password-update.dto';
import { UserUpdateDto } from './dto/user-update.dto';
import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private model: Model<UserDocument>) {}

  async create(userCreateDto: UserCreateDto): Promise<User> {
    return await new this.model({
      ...userCreateDto,
    }).save();
  }

  async getUser(): Promise<User[]> {
    return await this.model.find().exec();
  }

  async findOne(id: string): Promise<User> {
    return await this.model.findOne({ _id: id }).exec();
  }

  async delete(id: string): Promise<User> {
    return await this.model.findByIdAndDelete(id).exec();
  }

  async update(userUpdateDto: UserUpdateDto, id: string): Promise<User> {
    return await new this.model({
      ...userUpdateDto,
    }).updateOne({ _id: id });
  }

  async updatePassword(
    userPasswordUpdateDto: UserPasswordUpdateDto,
    id: string,
  ): Promise<User> {
    return await this.model.findByIdAndUpdate(
      { _id: id },
      {
        password: userPasswordUpdateDto.newPassword,
      },
    );
  }

  async findPassword(id: string): Promise<User> {
    return await this.model.findById(id).select('password').exec();
  }

  async findEmail(email: string): Promise<User> {
    return await this.model
      .findOne({ email: email })
      .select('name address contact.email password')
      .exec();
  }
}
