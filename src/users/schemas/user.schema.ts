import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { IContact } from '../interfaces/contact.interface';
import { ContactSchema } from './contact.schema';

export type UserDocument = User & Document;

@Schema()
export class User {
  readonly _id: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  gender: string;

  @Prop()
  address: string;

  @Prop({ type: [ContactSchema], default: [] })
  contact: IContact[];

  @Prop({ required: true, select: false })
  password: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
