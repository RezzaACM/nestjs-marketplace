import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ContactDocument = Contact & Document;

@Schema({ _id: false })
export class Contact {
  @Prop({ required: true })
  email: string;

  @Prop({ required: true, minlength: 3, maxlength: 15 })
  phone: string;
}

export const ContactSchema = SchemaFactory.createForClass(Contact);
