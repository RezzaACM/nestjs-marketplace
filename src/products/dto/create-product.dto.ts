import { IsNotEmpty } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  item: string;

  @IsNotEmpty()
  price: number;

  description: string;

  status: number;

  image: string;

  imageKey: string;
}
