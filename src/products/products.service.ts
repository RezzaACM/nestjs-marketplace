import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { Product, ProductDocument } from './schemas/product.schema';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private model: Model<ProductDocument>,
  ) {}

  async create(createProductDto: CreateProductDto): Promise<Product> {
    return await new this.model({
      ...createProductDto,
    }).save();
  }

  async findAll(): Promise<Product[]> {
    return await this.model.find({}).exec();
  }

  async findOnePrice(id: Types.ObjectId): Promise<Product> {
    return await this.model.findById(id).select('price').exec();
  }
}
