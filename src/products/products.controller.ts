import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateProductDto } from './dto/create-product.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productService: ProductsService) {}

  @Post()
  async createProduct(
    @Res() res: Response,
    @Body() createProductDto: CreateProductDto,
  ) {
    try {
      await this.productService.create(createProductDto);
      res.status(201).json({
        status: true,
        message: 'Successfully create product!',
      });
    } catch (error) {
      throw new HttpException(error.toString(), HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getProduct(@Res() res: Response) {
    try {
      const data = await this.productService.findAll();
      res.status(201).json({
        status: true,
        message: 'Successfully retrieve product!',
        data: data,
        total: data.length,
      });
    } catch (error) {
      throw new HttpException(
        error.toString(),
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
