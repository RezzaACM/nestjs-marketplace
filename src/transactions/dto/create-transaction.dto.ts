import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { Types } from 'mongoose';
import { CreateDetailTransactionDto } from './create-detail-transaction.dto';

export class CreateTransactionDto {
  user: Types.ObjectId;
  @ValidateNested()
  @Type(() => CreateDetailTransactionDto)
  details!: CreateDetailTransactionDto[];
  total: number;
}
