import { Types } from 'mongoose';

export class CreateDetailTransactionDto {
  product: Types.ObjectId;

  item_amount: number;

  subtotal: number;
}
