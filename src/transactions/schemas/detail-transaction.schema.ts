import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Product } from 'src/products/schemas/product.schema';

export type DetailTransactionDocument = DetailTransaction & Document;

@Schema()
export class DetailTransaction {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Product', required: true })
  product: Product;

  @Prop({ required: true, default: 0 })
  item_amount: number;

  @Prop({ default: 0 })
  subtotal: number;
}

export const DetailTransactionSchema =
  SchemaFactory.createForClass(DetailTransaction);
