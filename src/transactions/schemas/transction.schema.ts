import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { User } from 'src/users/schemas/user.schema';
import {
  DetailTransaction,
  DetailTransactionSchema,
} from './detail-transaction.schema';

export type TransactionDocument = Transaction & Document;

@Schema({ timestamps: true })
export class Transaction {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'User', required: true })
  user: User;

  @Prop({ type: [DetailTransactionSchema], default: [] })
  details: DetailTransaction[];

  @Prop()
  total: number;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
