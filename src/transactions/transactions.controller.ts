import { Body, Controller, Get, Post } from '@nestjs/common';
import { ProductsService } from 'src/products/products.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { TransactionsService } from './transactions.service';

@Controller('transactions')
export class TransactionsController {
  constructor(
    private readonly transactionService: TransactionsService,
    private readonly productService: ProductsService,
  ) {}

  @Post()
  async create(@Body() createTransactionDto: CreateTransactionDto) {
    let grandTotal = 0;
    for (let i = 0; i < createTransactionDto.details.length; i++) {
      const data = createTransactionDto.details[i];
      const product = await this.productService.findOnePrice(data.product);
      data.subtotal = product.price * data.item_amount;
      grandTotal += data.subtotal;
    }
    createTransactionDto.total = grandTotal;
    return await this.transactionService.create(createTransactionDto);
  }

  @Get()
  async getTransaction() {
    return await this.transactionService.findAll();
  }
}
