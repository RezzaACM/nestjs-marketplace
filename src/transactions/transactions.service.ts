import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { Transaction, TransactionDocument } from './schemas/transction.schema';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectModel(Transaction.name) private model: Model<TransactionDocument>,
  ) {}

  async create(
    createTransactionDto: CreateTransactionDto,
  ): Promise<Transaction> {
    return await new this.model({
      ...createTransactionDto,
    }).save();
  }

  async findAll(): Promise<Transaction[]> {
    return await this.model
      .find()
      .populate('user')
      .populate('details.product', 'item price description')
      .exec();
  }
}
