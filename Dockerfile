FROM node:12.19.0-alpine3.9 AS development

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY ./ .

EXPOSE 3000

RUN npm run build

# CMD ["node", "dist/main"]